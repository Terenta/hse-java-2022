import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите 3 числа (длину сторон a и b, а также угол между ними в градусах): ");
        final double a = scanner.nextDouble();
        final double b = scanner.nextDouble();
        final double angle = scanner.nextDouble();
        final double cSquared = Math.pow(a, 2.) + Math.pow(b, 2.) - 2. * a * b * Math.cos(Math.PI / 180. * angle);
        System.out.print("Длина третьей стороны: ");
        System.out.println(Math.sqrt(cSquared));
    }
}
