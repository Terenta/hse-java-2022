import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число и индекс бита: ");
        final int number = scanner.nextInt();
        final int index = scanner.nextInt();
        System.out.print(index);
        System.out.print("-ый бит числа ");
        System.out.print(number);
        System.out.print(": ");
        System.out.println((number >> index) & 1);
    }
}
