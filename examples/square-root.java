import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        BigDecimal a = scanner.nextBigDecimal();
        BigDecimal res = BigDecimal.valueOf(1);

        for (int i = 0; i < 25; i ++)
            res = BigDecimal.valueOf(0.5).multiply(res.add(a.divide(res, MathContext.DECIMAL128)));

        System.out.println(res);
    }
}
