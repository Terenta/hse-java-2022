import java.util.ArrayList;


public class Scatter {
    public Scatter () { _points = new ArrayList<Coordinates>(); }
    public Coordinates get (final int i) { return _points.get(i); }
    public void set (final int i, final Coordinates point) { _points.set(i, point); }
    public void add (final Coordinates point) { _points.add(point); }
    public void remove (final int i) { _points.remove(i); }
    public int size () { return _points.size(); }

    public Coordinates center () {
        Coordinates value = new Coordinates(0., 0.);

        for (int i = 0; i < size(); i ++)
            value = value.add(get(i));

        return new Coordinates(value.get_x() / size(), value.get_y() / size());
    }

    private final ArrayList<Coordinates> _points;
}
