import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    static Path workdir = null;

    public static void main (String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Expected workdir and port number in args");
            System.exit(1);
        }

        workdir = Path.of(args[0]).normalize();
        tryCreateWorkdir(workdir);

        HttpServer server = HttpServer.create(new InetSocketAddress(Integer.parseInt(args[1])), 0);
        server.createContext("/", new GeneralHandler());
        server.setExecutor(null);
        server.start();
    }

    private static void tryCreateWorkdir (final Path path) {
        if (Files.exists(path))
            return;

        try {
            Files.createDirectory(path);
        } catch (final IOException exception) {
            System.out.printf("Can't create workdir at %s\n", path);
            exception.printStackTrace();
            System.exit(1);
        }

        System.out.printf("Created workdir at %s\n", path);
    }

    private static class GeneralHandler implements HttpHandler {
        private static final Map<Integer, String> HTTP_STATUS_CODE_MAP = Map.of(
            200, "Ok",
            201, "Created",
            400, "Bad Request",
            404, "Not Found",
            405, "Method Not Supported",
            500, "Internal Server Error"
        );

        private static void respond (
            final HttpExchange exchange,
            final int code,
            String response
        ) throws IOException {
            response = String.format(
                "<h1>%d %s</h1>%s%s",
                code,
                HTTP_STATUS_CODE_MAP.get(code),
                response,
                code / 100 != 2 ? "<a href=\"/\">Home</a>" : ""
            );

            exchange.sendResponseHeaders(code, response.length());

            final OutputStream responseBody = exchange.getResponseBody();
            responseBody.write(response.getBytes());
            responseBody.close();
        }

        private static void appendBodyToFile (
            final HttpExchange exchange,
            final Path file
        ) throws IOException {
            final FileOutputStream outputStream = new FileOutputStream(file.toFile(), true);
            outputStream.write(exchange.getRequestBody().readAllBytes());
            outputStream.close();
        }

        @Override
        public void handle (final HttpExchange exchange) throws IOException {
            try {
                final Path rawRequestPath = Path.of(exchange.getRequestURI().getPath()).normalize();
                final Path requestPath = Path.of(IntStream
                    .range(0, rawRequestPath.getNameCount())
                    .mapToObj(rawRequestPath::getName)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.joining("/")));

                final Path parentPath = Path.of(
                    requestPath.getNameCount() <= 1 ?
                    "" :
                    IntStream
                        .range(0, requestPath.getNameCount() - 1)
                        .mapToObj(requestPath::getName)
                        .map(Path::getFileName)
                        .map(Path::toString)
                        .collect(Collectors.joining("/"))
                );

                final Path file = workdir.resolve(requestPath);
                final Path parentFile = workdir.resolve(parentPath);

                if (
                    List.of("GET", "PATCH", "DELETE").contains(exchange.getRequestMethod()) &&
                    !Files.exists(file, LinkOption.NOFOLLOW_LINKS)
                )
                    respond(exchange, 404, String.format("<p>File /%s not found</p>", requestPath));

                else if (
                    List.of("POST", "PUT").contains(exchange.getRequestMethod()) &&
                    Files.exists(file, LinkOption.NOFOLLOW_LINKS)
                )
                    respond(exchange, 400, String.format("<p>File /%s already exists</p>", requestPath));

                else if (
                    List.of("POST", "PUT").contains(exchange.getRequestMethod()) &&
                    !Files.exists(parentFile)
                )
                    respond(exchange, 400, "<p>Parent file does not exist</p>");

                else if (exchange.getRequestMethod().equals("GET")) {
                    final String contents;

                    if (Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))
                        contents = String.format("<p>Regular file contents:</p><p>%s</p>", Files.readString(file));

                    else if (Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS))
                        contents = String.format(
                            "<p>Directory contents:</p><ul>%s</ul>",
                            Files
                                .list(file)
                                .map(path -> String.format(
                                    "<li><a href=\"/%s\">%s</a></li>",
                                    requestPath.resolve(path.getFileName()),
                                    path.getFileName() + (Files.isDirectory(path) ? "/" : "")
                                )).collect(Collectors.joining())
                        );

                    else
                        contents = "<p>File type is not supported</p>";

                    respond(exchange, 200, String.format(
                        "<a href=\"/%s\">Return to parent folder</a><pre>%s</pre>",
                        parentPath,
                        contents
                    ));
                }

                else if (exchange.getRequestMethod().equals("POST")) {
                    Files.createFile(file);
                    appendBodyToFile(exchange, file);
                    respond(exchange, 201, "<p>Regular file has been created</p>");
                }

                else if (exchange.getRequestMethod().equals("PATCH")) {
                    if (!Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))
                        respond(exchange, 400, "<p>Requested file is not a regular file</p>");

                    else {
                        appendBodyToFile(exchange, file);
                        respond(exchange, 200, "<p>Regular file has been patched</p>");
                    }
                }

                else if (exchange.getRequestMethod().equals("PUT")) {
                    Files.createDirectory(file);
                    respond(exchange, 201, "<p>Directory has been created</p>");
                }

                else if (exchange.getRequestMethod().equals("DELETE")) {
                    final boolean deleted = Files.walk(file)
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .allMatch(File::delete);

                    if (!deleted)
                        respond(exchange, 500, "<p>File hasn't been deleted</p>");

                    else {
                        tryCreateWorkdir(workdir);
                        respond(exchange, 200, "<p>File has been deleted</p>");
                    }
                }

                else
                    respond(exchange, 405, "");
            } catch (final Exception exception) {
                respond(exchange, 500, String.format("<p>%s</p>", exception));
            }
        }
    }
}
