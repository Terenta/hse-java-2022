import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int n = scanner.nextInt();

        final double[] x = new double[n];
        final double[] y = new double[n];

        for (int i = 0; i < n; i ++) {
            x[i] = scanner.nextDouble();
            y[i] = scanner.nextDouble();
        }

        boolean isConvexOrientedTop = true;

        if (n >= 3)
            for (int firstPointIndex = 0; firstPointIndex < n; firstPointIndex ++) {
                final int secondPointIndex = (firstPointIndex + 1) % n;
                final int thirdPointIndex = (firstPointIndex + 2) % n;
                final double v1x = x[secondPointIndex] - x[firstPointIndex];
                final double v1y = y[secondPointIndex] - y[firstPointIndex];
                final double v2x = x[thirdPointIndex] - x[secondPointIndex];
                final double v2y = y[thirdPointIndex] - y[secondPointIndex];

                if (crossProduct(v1x, v1y, v2x, v2y) <= 0) {
                    isConvexOrientedTop = false;
                    break;
                }
            }

        System.out.println(isConvexOrientedTop);
    }

    public static double dotProduct (final double x1, final double y1, final double x2, final double y2) {
        return x1 * x2 + y1 * y2;
    }

    public static double crossProduct (final double x1, final double y1, final double x2, final double y2) {
        return x1 * y2 - y1 * x2;
    }
}
