import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int n = scanner.nextInt();
        System.out.println(factorial(n));
    }

    public static BigInteger factorial (final int n) {
        return n <= 1 ? BigInteger.valueOf(1) : factorial(n - 1).multiply(BigInteger.valueOf(n));
    }
}
